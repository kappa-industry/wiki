<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indexes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('parent_id')->nullable();
            $table->unsignedInteger('entry_id');
            $table->unsignedInteger('card_id');
            $table->string('name', 128);
            $table->unsignedInteger('order_id');
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('indexes');
            $table->foreign('entry_id')->references('id')->on('entries');
            $table->foreign('card_id')->references('id')->on('cards');
            $table->index('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indexes');
    }
}
