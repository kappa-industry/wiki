<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('name', 64);
            $table->string('email', 64)->unique();
            $table->string('phone', 20)->nullable()->unique();
            $table->string('password_hash', 128);
            $table->string('avatar', 255)->nullable();
            $table->addColumn('tinyInteger', 'gender', ['unsigned' => true, 'default' => 2]);
            $table->dateTime('birthday')->nullable();
            $table->string('school', 128)->nullable();
            $table->string('company', 128)->nullable();
            $table->timestamps();

            $table->index('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
