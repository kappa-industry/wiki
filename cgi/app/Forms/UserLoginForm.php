<?php

namespace App\Forms;

use App\User;
use Illuminate\Support\Facades\Hash;
use App\Rules\Captcha as RuleCaptcha;


class UserLoginForm extends Form
{
    public function setRules(): array
    {
        return [
            'email' => 'required|email',
            'password' => 'required|between:6,32',
            // 'captcha' => ['required', 'size:6', new RuleCaptcha()],
        ];
    }

    public function setMessages(): array
    {
        return [
            'email.required' => '电子邮件不能为空',
            'email.email' => '电子邮件格式错误',
            'password.*' => '账号或密码错误',
            'captcha.required' => '验证码不能为空',
            'captcha.size' => '验证码格式错误'
        ];
    }
}
