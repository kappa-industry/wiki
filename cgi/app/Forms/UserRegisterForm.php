<?php

namespace App\Forms;

use App\User;
use Illuminate\Support\Facades\Hash;
use App\Rules\Captcha;
use App\Services\AuthService;


class UserRegisterForm extends Form
{
    public function setRules(): array
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|between:6,32',
            'phone' => 'nullable|between:5,13',
            'gender' => 'nullable|integer|max:2|min:0',
            'captcha' => ['required', 'size:6', new Captcha()],
        ];
    }

    public function setMessages(): array
    {
        return [
            'name.required' => '名字不能为空',
            'email.required' => '电子邮件不能为空',
            'email.email' => '电子邮件格式错误',
            'email.unique' => '这个电子邮件已经被注册了',
            'phone.*' => '电话号码格式错误',
            'gender.*' => '请选择正确的性别',
            'captcha.required' => '验证码不能为空',
            'captcha.size' => '验证码格式错误'
        ];
    }

    /**
     * Save a user from attributes.
     * @return bool
     */
    public function save(): bool
    {
        /** @var AuthService $auth */
        $auth = app(AuthService::class);
        $user = new User($this->getAttributes());
        $user->withPassword($this->password);
        return $user->save();
    }
}
