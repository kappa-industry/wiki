<?php

namespace App\Forms;

use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Validator;

abstract class Form
{
    /** @var array $attributes */
    protected $attributes = [];

    /**
     * Regist request attributes.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $data = $request->all();
        $this->setAttributes($data);
    }

    /**
     * Set messages for input fields.
     *
     * @return array
     */
    abstract public function setMessages(): array;

    /**
     * Set ruls for input fields.
     *
     * @return array
     */
    abstract public function setRules(): array;

    /**
     * Get validator.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(): Validator
    {
        $data = $this->getAttributes();
        $rules = $this->setRules();
        $messages = $this->setMessages();
        $validator = validator($data, $rules, $messages);
        return $validator;
    }

    /**
     * Get attributes.
     *
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * Set attributes.
     *
     * @param array $attributes
     * @return self
     */
    public function setAttributes(array $attributes): self
    {
        $this->attributes = array_merge(
            $this->attributes,
            $attributes
        );
        // print_r($attributes);exit();
        foreach ($attributes as $key => $value) {
            $this->$key = $value;
        }
        return $this;
    }
}
