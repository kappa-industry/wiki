<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Redis;
use Illuminate\Http\Request;
use App\Services\AuthService;

/**
 * @property AuthService $authService
 */
class Captcha implements Rule
{
    public function __construct()
    {
        $this->authService = app(AuthService::class);
        $this->request = app(Request::class);
    }
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $guardKey = $this->authService->getGuardKey('captcha');
        $captcha = Redis::get($guardKey);
        return (bool)($value === $captcha);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return '验证码错误';
    }
}