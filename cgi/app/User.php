<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'password_hash', 'avatar', 'gender',
        'birthday', 'school', 'company', 'created_at', 'updated_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password_hash',
    ];

    /**
     * Make password_hash for user.
     *
     * @param  string  $password
     * @return bool
     */
    public function withPassword(string $password)
    {
        $this->password_hash = password_hash($password, PASSWORD_DEFAULT);
        return $this;
    }

    /**
     * Verify the user password.
     * 
     * @param string $password
     * @return bool
     */
    public function passwordVerify(string $password)
    {
        return password_verify($password, $this->password_hash);
    }
}
