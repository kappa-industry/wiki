<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Gregwar\Captcha\CaptchaBuilder;
use Illuminate\Support\Facades\Redis;
use App\Forms\UserRegisterForm;
use App\Forms\UserLoginForm;
use App\Services\UserService;
use App\Services\AuthService;
use App\Services\JwtService;


/**
 * Build responses for user.
 * 
 * @property Request $request
 */
class AuthController extends Controller
{
    /** @var Request $request */
    public $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function ping(JwtService $jwtService)
    {
        $payload = $jwtService->getPayload();
        $exp = $payload->exp;
        $position = env('JWT_REFRESH_POSITION');
        $result = $exp - (time() - $position);
        return new Response($result, 200);
    }

    /**
     * Register new user.
     * @link auth/register
     *
     * @return JsonResponse
     */
    public function register(): JsonResponse
    {
        $form = new UserRegisterForm($this->request);
        $validator = $form->validator();
        if ($validator->fails()) {
            return new JsonResponse($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if ($form->save()) {
            return new JsonResponse(['message' => '创建用户成功！'], Response::HTTP_CREATED);
        }
        return new JsonResponse(['message' => '创建用户失败，请联系站长～'], Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * Login and return an auth token.
     * @link auth/login
     *
     * @param UserService $userService
     * @param JwtService $jwtService
     * @return JsonResponse
     */
    public function login(UserService $userService, JwtService $jwtService): JsonResponse
    {
        $form = new UserLoginForm($this->request);
        $validator = $form->validator();
        if ($validator->fails()) {
            return new JsonResponse($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        /** @var User $user */
        $user = $userService->findByEmail($form->email)->first();
        $id = $user->id;
        if ($user && $user->passwordVerify($form->password)) {
            $token = $jwtService->createToken($user);
            return new JsonResponse(compact('token', 'id'));
        }
        return new JsonResponse(['password' => ['账号或密码错误']], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * Check email exist.
     * @link auth/email-exist
     *
     * @return Response
     */
    public function emailExist(): Response
    {
        $this->validate($this->request, [
            'email' => 'required|email'
        ], [
            'email.required' => '电子邮件不能为空',
            'email.email' => '电子邮件格式错误'
        ]);
        $email = $this->request->input('email');
        $count = User::query()->where('email', $email)->count();
        if ($count > 0) {
            return new Response($count);
        }
        return new Response($count, Response::HTTP_NOT_FOUND);
    }

    /**
     * Output captcha image.
     * @link auth/captcha
     * 
     * @param AuthService $authService
     *
     * @return Response
     */
    public function captcha(AuthService $authService): Response
    {
        $builder = new CaptchaBuilder(6);
        $builder->build();
        $phrase = $builder->getPhrase();

        $guardKey = $authService->getGuardKey('captcha');
        Redis::setex($guardKey, 300, $phrase);

        return new Response($builder->get(), Response::HTTP_OK, [
            'content-type' => 'image/jpeg'
        ]);
    }
}
