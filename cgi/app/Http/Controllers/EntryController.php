<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Build responses for entry.
 * @property Request $request
 */
class EntryController extends Controller
{
    /** @var Request $request */
    public $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function list()
    {
        $a = $this->request->input('a');
        $user = $this->request->user();
        return new JsonResponse($user);
    }
}
