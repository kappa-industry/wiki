<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserOptionController extends Controller
{
    /** @var Request $request */
    public $request;

   /**
    * @param Request $request
    */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function write()
    {
        return new Response();
    }
}
