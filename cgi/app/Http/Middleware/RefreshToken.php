<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use App\Services\JwtService;

class RefreshToken
{
    /**
     * The authentication service instance.
     *
     * @var \App\Services\Auth
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \App\Services\Auth  $auth
     * @return void
     */
    public function __construct(JwtService $jwtService)
    {
        $this->auth = $jwtService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return Response
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if ($this->auth->isExpires()) {
            $response = $this->auth->setAuthorizationResponse($response);
        }
        return $response;
    }
}
