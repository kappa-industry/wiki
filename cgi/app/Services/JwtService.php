<?php
namespace App\Services;

use App\User;
use Firebase\JWT\JWT;
use Laravel\Lumen\Application;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class JwtService extends Service
{
    /** @static string  */
    const SCHEME = 'Bearer';

    /**
     * Authorization items.
     * @var array $items
     */
    protected $items = [];

    /**  @var array|null $payload */
    protected $payload;

    /** @var string $tokenKey */
    public $tokenKey;

    /** @var string $algorithm */
    public $algorithm;

    /** @var integer $expPosition */
    public $expPosition;

    /** @var integer $refreshPosition */
    public $refreshPosition;

    /**
     * For Auth provider boot.
     * @param string $token
     * @return bool
     */
    public function loadFromToken($token)
    {
        $this->items = preg_split('/\s+/', $token);
    }

    /**
     * Format the authorization scheme.
     * @param string $value
     * @return string
     */
    public function formatScheme(string $value)
    {
        $scheme = strtolower($value);
        return ucfirst($scheme);
    }

    /**
     * Get http authorization scheme.
     * @return string
     */
    public function getScheme()
    {
        return $this->formatScheme($this->items[0]);
    }

    /**
     * Validate for http authorization scheme.
     * @param string $scheme
     * @return boolean
     */
    public function isScheme(string $scheme)
    {
        $currentScheme = $this->getScheme();
        $validScheme = $this->formatScheme($scheme);
        return (bool)($currentScheme === $validScheme);
    }

    /**
     * Current authorization scheme is`Bearer`?
     * @return boolean
     */
    public function isBearer()
    {
        return $this->isScheme(static::SCHEME);
    }

    /**
     * Get authorization token
     * @return string
     */
    public function getToken()
    {
        return (string)$this->items[1];
    }

    /**
     * Get jwt payload.
     * @return object
     */
    public function getPayload()
    {
        if (is_null($this->payload)) {
            JWT::$leeway = $this->refreshPosition;
            try {
                $this->payload = JWT::decode(
                    $this->getToken(),
                    $this->tokenKey,
                    [$this->algorithm]
                );
            } catch (\Throwable $e) {
                throw new UnauthorizedHttpException('');
            }
        }
        return $this->payload;
    }

    /**
     * Create an authorization token.
     * @param User $user
     * @return string
     */
    public function createToken(User $user)
    {
        $data['id'] = $user->id;
        $data['name'] = $user->name;
        $payload = [
            'iss' => 'omytty',
            'data' => $data
        ];
        $payload = $this->refreshDate($payload);
        return JWT::encode($payload, $this->tokenKey, $this->algorithm);
    }

    /**
     * Refresh jwt payload expires date.
     * @param array|object $payload
     * @return object
     */
    public function refreshDate($payload)
    {
        $payload = (object)$payload;
        $time = time();
        $payload->iat = $time;
        $payload->exp = $time + $this->expPosition;
        return $payload;
    }

    /**
     * Refresh current payload.
     * @return object
     */
    public function refreshCurrentPayload()
    {
        $payload = $this->getPayload();
        $newPayload = clone $payload;
        $this->refreshDate($newPayload);
        return $newPayload;
    }

    /**
     * If jwt token expiresd?
     * @return boolean
     */
    public function isExpires()
    {
        $payload = $this->getPayload();
        return (bool) ($payload->exp <= time());
    }

    /**
     * @param Response $response
     * @return Response
     */
    public function setAuthorizationResponse(Response $response)
    {
        $payload = $this->refreshCurrentPayload();
        $token = JWT::encode($payload, $this->tokenKey, $this->algorithm);
        $response->header('Authorization', $token);
        return $response;
    }
}
