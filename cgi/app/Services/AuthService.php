<?php
namespace App\Services;

use App\User;
use Carbon\Carbon;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Hash;

class AuthService extends Service
{
    /** @var string $guardKey */
    protected $guardKey;

    /**
     * Get guard key.
     *
     * @param string $ip
     * @param string $key
     * @return string
     */
    public function getGuardKey(string $key)
    {
        $request = app(Request::class);
        $ip = $request->getClientIp();
        $this->guardKey = $this->guardKey ?? md5($ip);
        return $this->guardKey . ':' . $key;
    }
}
