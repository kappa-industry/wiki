<?php

namespace App\Services;

use Laravel\Lumen\Application;

/**
 * @property \Laravel\Lumen\Application $app
 */
abstract class Service
{
    /**
     * Regist app.
     *
     * @param Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function __get($name)
    {
        $service = __NAMESPACE__ . '\\' . ucfirst($name);
        return $this->app[$service];
    }
}
