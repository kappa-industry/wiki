<?php

namespace App\Services;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Builder;

class UserService extends Service
{
    /**
     * Find user by email.
     * @param string $email
     * @return Builder
     */
    public function findByEmail(string $email): Builder
    {
        return User::query()->where(compact('email'));
    }
}
