<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;
use App\Services\JwtService;
use App\Services\AuthService;

class AuthServiceProvider extends AbstractProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->quickRegister(JwtService::class, function($jwtService) {
            $jwtService->tokenKey = env('JWT_KEY');
            $jwtService->algorithm = env('JWT_ALG');
            $jwtService->expPosition = env('JWT_EXP_POSITION');
            $jwtService->refreshPosition = env('JWT_REFRESH_POSITION');
            return $jwtService;
        });
        $this->quickRegister(AuthService::class);
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot(JwtService $auth)
    {
        $this->app['auth']->viaRequest('api', function ($request) use ($auth) {
            $token = $request->headers->get('Authorization', false);
            $auth->loadFromToken($token);
            try {
                $payload = $auth->getPayload();
                if ($auth->isBearer()) {
                    $payload = $auth->getPayload();
                    $userId = $payload->data->id;
                    return User::query()->whereKey(1)->first();
                }
            } catch (\Throwable $e) {
                return null;
            }
        });
    }
}
