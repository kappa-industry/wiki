<?php

namespace App\Providers;

use App\Services\ToolService;

class AppServiceProvider extends AbstractProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->quickRegister(ToolService::class);
    }
}
