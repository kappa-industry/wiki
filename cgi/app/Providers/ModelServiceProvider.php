<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use App\Services\UserService;

class ModelServiceProvider extends AbstractProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->quickRegister(UserService::class);
    }
}
