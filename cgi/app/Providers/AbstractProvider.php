<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

abstract class AbstractProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    abstract public function register();

    /**
     * @param string $class
     * @param \Closure|null $closure
     */
    protected function quickRegister(string $class, ?\Closure $closure = null)
    {
        $this->app->singleton($class, function($app) use ($class, $closure) {
            $service = new $class($app);
            if (method_exists($closure, '__invoke')) {
                return $closure($service);
            }
            return $service;
        });
    }
}
