<?php

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Redis;
use Predis\Client;
use Illuminate\Encryption\EncryptionServiceProvider;
use App\Services\JwtService;


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group([
    'prefix' => 'api',
    'middleware' => ['auth', 'refreshToken']
], function () use ($router) {

});

$router->group([
    'prefix' => 'api'
], function () use ($router) {

    $router->post('auth/register', ['uses' => 'AuthController@register', 'as' => 'auth.register']);
    $router->post('auth/login', ['uses' => 'AuthController@login', 'as' => 'auth.login']);
    $router->get('auth/email-exist', ['uses' => 'AuthController@emailExist', 'as' => 'auth.emailExist']);
    $router->get('auth/captcha', ['uses' => 'AuthController@captcha', 'as' => 'auth.captcha']);

    $router->get('/key-mock', function() {
        return str_random(32);
    });

});

$router->group([
    'prefix' => 'api',
    'middleware' => ['auth', 'refreshToken']
], function () use ($router) {
    $router->get('auth/ping', ['uses' => 'AuthController@ping', 'as' => 'auth.ping']);

    $router->get('entry/list', ['uses' => 'EntryController@list', 'as' => 'entry.list']);

    $router->post('user/option', ['uses' => 'UserOptionController@write', 'as' => 'user.option.write']);
});
