import { FormControl, FormGroup, ValidationErrors } from "@angular/forms";

export abstract class FormComponent {

  /** @var form current form */
  abstract form: FormGroup;

  /**
   * 
   * @param name FormControl name
   * @param type error type (e.g required)
   */
  isInvalid(name: string, type: any): Boolean {
    const control: FormControl = this.form[name];
    if (control.invalid && (control.dirty || control.touched)) {
      if (type) {
        return Boolean(control.errors[type]);
      }
      return control.invalid;
    }
  }

  /**
   * If has async error.
   * @param name 
   */
  hasAsyncError(name: string): Boolean {
    const control: FormControl = this.form[name];
    if (control.invalid) {
      return Boolean(control.errors['async']);
    }
    return false;
  }

  getAsyncError(name: string): any {
    const control: FormControl = this.form[name];
    if (control.invalid) {
      return control.errors['async'];
    }
    return null;
  }

  /**
   * Async error handler.
   * Every invalid form control has an `async` error.
   * 
   * @param promise 
   * @param callback 
   * 
   * @return Promise
   */
  asyncErrorHandle(promise: Promise<any>, callback: Function=null): Promise<any> {
    return promise.catch(error => {
      if (error.status === 422) {
        for (const e in error.error) {
          const control: FormControl = this.form[e];
          const errors: ValidationErrors = {
            async: error.error[e][0]
          };
          control.setErrors(errors);
        }
      }
      if (callback instanceof Function) {
        const func = callback.bind(this);
        func(error);
      }
    });
  }

}