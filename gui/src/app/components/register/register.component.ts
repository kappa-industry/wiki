import { Component, OnInit } from '@angular/core';
import { RegistData } from '../../interfaces/auth.interface';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FormControl, ValidationErrors } from '@angular/forms';
import { RegisterForm } from '../../forms/register.form';
import { FormComponent } from '../form.component';
import { ToolService } from '../../services/tool.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent extends FormComponent implements OnInit {

  captchaUrl: string;

  form: RegisterForm;

  constructor(
    public authService: AuthService,
    public toolService: ToolService,
    public router: Router
  ) {
    super();
  }

  ngOnInit() {
    this.form = new RegisterForm();
    this.refreshCaptcha();
  }

  /**
   * Refresh the captcha image.
   */
  refreshCaptcha() {
    this.captchaUrl = this.toolService.refreshCaptchaUrl();
  }

  /**
   * Submit for register.
   * @return Promise
   */
  onSubmit() {
    if (this.form.valid) {
      let promise: Promise<any> = this.authService.register(this.form.value);
      promise = promise.then((data) => {
        alert('注册成功，欢迎入驻！');
        this.router.navigateByUrl('/login');
      });
      return this.asyncErrorHandle(promise, error => {
        this.refreshCaptcha();
      });
    }
  }

}
