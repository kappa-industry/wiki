import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolsComponent } from './tools.component';
import { TransverseNavComponent } from './transverse-nav/transverse-nav.component';
import { RouterModule } from '@angular/router';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { LinkListComponent } from './link-list/link-list.component';
import { EllipseButtonComponent } from './ellipse-button/ellipse-button.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    AngularFontAwesomeModule
  ],
  declarations: [
    ToolsComponent,
    TransverseNavComponent,
    LinkListComponent,
    EllipseButtonComponent
  ],
  exports: [
    ToolsComponent,
    TransverseNavComponent,
    LinkListComponent,
    EllipseButtonComponent
  ]
})
export class ToolsModule { }
