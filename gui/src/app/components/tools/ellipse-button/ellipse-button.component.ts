import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'tool-ellipse-button',
  templateUrl: './ellipse-button.component.html',
  styleUrls: ['./ellipse-button.component.scss']
})
export class EllipseButtonComponent implements OnInit {

  @Input() color: string;
  @Input() icon: string;
  @Input() name: string;

  constructor() { }

  ngOnInit() {
  }

}
