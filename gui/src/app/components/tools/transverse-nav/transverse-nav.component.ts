import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'tool-transverse-nav',
  templateUrl: './transverse-nav.component.html',
  styleUrls: ['./transverse-nav.component.scss']
})
export class TransverseNavComponent implements OnInit {

  @Input() items: Array<object>;

  constructor() { }

  ngOnInit() {
  }

}
