import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransverseNavComponent } from './transverse-nav.component';

describe('TransverseNavComponent', () => {
  let component: TransverseNavComponent;
  let fixture: ComponentFixture<TransverseNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransverseNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransverseNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
