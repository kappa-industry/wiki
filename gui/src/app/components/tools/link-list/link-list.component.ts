import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'tool-link-list',
  templateUrl: './link-list.component.html',
  styleUrls: ['./link-list.component.scss']
})
export class LinkListComponent implements OnInit {

  @Input() items: Array<object>;
  @Input() class: string;
  @Input() icon: string;

  constructor() { }

  ngOnInit() {
  }

}
