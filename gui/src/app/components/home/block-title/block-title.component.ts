import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-home-block-title',
  templateUrl: './block-title.component.html',
  styleUrls: ['./block-title.component.scss']
})
export class BlockTitleComponent implements OnInit {

  @Input() icon: string;
  @Input() text: string;
  @Input() class: string;
  @Input('more-href') moreHref: string;

  constructor() { }

  ngOnInit() {
  }

}
