import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { HomeComponent } from './home.component';
import { BulletinComponent } from './bulletin/bulletin.component';
import { BlockTitleComponent } from './block-title/block-title.component';
import { BlockBodyComponent } from './block-body/block-body.component';
import { SimpleBlockComponent } from './simple-block/simple-block.component';

export const routes: Routes = [
  { path: '', component: HomeComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    AngularFontAwesomeModule
  ],
  declarations: [
    HomeComponent,
    BulletinComponent,
    BlockTitleComponent,
    BlockBodyComponent,
    SimpleBlockComponent
  ],
  exports: [
    HomeComponent
  ]
})
export class HomeModule { }
