import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-home-simple-block',
  templateUrl: './simple-block.component.html',
  styleUrls: ['./simple-block.component.scss']
})
export class SimpleBlockComponent implements OnInit {

  @Input() title: string;
  @Input() text: string;
  @Input() href: string;

  constructor() { }

  ngOnInit() {
  }

}
