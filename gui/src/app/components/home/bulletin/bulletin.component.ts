import { Component, OnInit, Input } from '@angular/core';
import { UserOptionService } from '../../../services/user-option.service';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-home-bulletin',
  templateUrl: './bulletin.component.html',
  styleUrls: ['./bulletin.component.scss']
})
export class BulletinComponent implements OnInit {

  @Input() img: string;
  @Input() header: string;
  @Input() text: string;
  @Input() hide: Boolean;

  constructor(
    public authService: AuthService,
    public userOptionService: UserOptionService
  ) { }

  ngOnInit() {
  }

  close() {
    this.userOptionService.diableBulletin();
  }

  isHidden(): Boolean {
    return !this.userOptionService.options.displayBulletin;
  }

}
