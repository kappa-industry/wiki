import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-home-block-body',
  templateUrl: './block-body.component.html',
  styleUrls: ['./block-body.component.scss']
})
export class BlockBodyComponent implements OnInit {

  @Input() entry: string;
  @Input() href: string;
  @Input() text: string;
  @Input() user: string;
  @Input() userUrl: string;

  constructor() { }

  ngOnInit() {
  }

}
