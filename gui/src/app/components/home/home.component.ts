import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { UserOptionService } from '../../services/user-option.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    public authService: AuthService
  ) { }

  ngOnInit() {
    // this.userOptionService.fetchOptions();
  }

  pingHandle() {
    this.authService.ping();
  }

}
