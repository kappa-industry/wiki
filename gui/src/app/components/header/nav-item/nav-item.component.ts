import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'header-nav-item',
  templateUrl: './nav-item.component.html',
  styleUrls: ['./nav-item.component.scss']
})
export class NavItemComponent implements OnInit {

  @Input() name: string;
  @Input() active: boolean;
  @Input() icon: string;
  @Input() href: string;

  constructor() { }

  ngOnInit() {
  }

}
