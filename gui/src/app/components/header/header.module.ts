import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header.component';
import { LogoComponent } from './logo/logo.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NavItemComponent } from './nav-item/nav-item.component';
import { SearchFormComponent } from './search-form/search-form.component';
import { SessionBoxComponent } from './session-box/session-box.component';
import { RouterModule, Routes } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    AngularFontAwesomeModule,
    RouterModule
  ],
  declarations: [
    HeaderComponent,
    LogoComponent,
    NavItemComponent,
    SearchFormComponent,
    SessionBoxComponent
  ],
  exports: [
    HeaderComponent,
  ]
})
export class HeaderModule { }
