import { Component, OnInit } from '@angular/core';
import { APP_NAME_EN } from '../../../constants/client.const';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss']
})
export class LogoComponent implements OnInit {

  name = APP_NAME_EN;

  constructor() { }

  ngOnInit() {
  }

}
