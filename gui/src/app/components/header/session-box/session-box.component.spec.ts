import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SessionBoxComponent } from './session-box.component';

describe('SessionBoxComponent', () => {
  let component: SessionBoxComponent;
  let fixture: ComponentFixture<SessionBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SessionBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SessionBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
