import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { LoginData } from '../../../interfaces/auth.interface';

@Component({
  selector: 'header-session-box',
  templateUrl: './session-box.component.html',
  styleUrls: ['./session-box.component.scss']
})
export class SessionBoxComponent implements OnInit {

  @Input() isLogin: Boolean;

  constructor(
    public authService: AuthService
  ) { }

  ngOnInit() {
  }

  /**
   * Show collect page.
   */
  collect() {
    const data: LoginData = {
      email: 'blldxt@aliyun.com',
      password: '',
      captcha: 'qweqwe'
    };
    const txt = this.authService.login(data);
    console.log(txt);
  }

}
