import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-wiki-card',
  templateUrl: './wiki-card.component.html',
  styleUrls: ['./wiki-card.component.scss']
})
export class WikiCardComponent implements OnInit {

  @Input() id: number;
  @Input() href: string;
  @Input() title: string;
  @Input() image: string;
  @Input() content: string;
  @Input() user: string;
  @Input() userUrl: string;
  @Input() datetime: string;
  @Output() collect = new EventEmitter<any>();
  @Output() watch = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  onCollect(event: MouseEvent) {
    event.preventDefault();
    this.collect.emit(this.id);
  }

  onWatch(event: MouseEvent) {
    event.preventDefault();
    this.watch.emit(this.id);
  }

}
