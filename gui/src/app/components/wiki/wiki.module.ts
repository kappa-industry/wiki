import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WikiComponent } from './wiki.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { TreeModule } from 'angular-tree-component';
import { SwitchNavbarComponent } from './switch-navbar/switch-navbar.component';
import { WikiCardComponent } from './wiki-card/wiki-card.component';
import { ToolsModule } from '../tools/tools.module';
import { AnalysisListComponent } from './analysis-list/analysis-list.component';
import { SinglePageComponent } from './single-page/single-page.component';
import { HeaderComponent } from './single-page/header/header.component';
import { ToolBarComponent } from './single-page/tool-bar/tool-bar.component';
import { BodyComponent } from './single-page/body/body.component';
import { IndexComponent } from './single-page/index/index.component';
import { BuilderComponent } from './builder/builder.component';
import { EditorComponent } from './builder/editor/editor.component';
import { BlockComponent } from './builder/editor/block/block.component';
import { MarkdownModule } from 'ngx-markdown';
import { CodeModeComponent } from './builder/editor/code-mode/code-mode.component';
import { MarkdownModeComponent } from './builder/editor/markdown-mode/markdown-mode.component';

export const routes: Routes = [
  { path: 'wiki', component: WikiComponent },
  { path: 'wiki/:id', component: SinglePageComponent },
  { path: 'wiki-builder', component: BuilderComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TreeModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFontAwesomeModule,
    MarkdownModule.forChild(),
    ToolsModule,
  ],
  declarations: [
    WikiComponent,
    SwitchNavbarComponent,
    WikiCardComponent,
    AnalysisListComponent,
    SinglePageComponent,
    HeaderComponent,
    ToolBarComponent,
    BodyComponent,
    IndexComponent,
    BuilderComponent,
    EditorComponent,
    BlockComponent,
    CodeModeComponent,
    MarkdownModeComponent
  ],
  exports: [
    WikiComponent,
    WikiCardComponent
  ]
})
export class WikiModule { }
