import { Component, OnInit } from '@angular/core';
import { ToolService } from '../../services/tool.service';
import { TAB_RECOMMEND, TAB_NEW, TAB_TOOLS } from './switch-navbar/constants';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-wiki',
  templateUrl: './wiki.component.html',
  styleUrls: ['./wiki.component.scss']
})
export class WikiComponent implements OnInit {

  currentTab: number;

  entries: Array<object> = [
    {id: 1},
    {id: 2},
    {id: 3}
  ];

  writeDockItems: Array<object> = [
    {href: '/wiki-builder', name: '新词条', icon: 'file-o'},
    {href: '/', name: '记笔记', icon: 'pencil'},
    {href: '/', name: '时间轴', icon: 'clone'}
  ];

  readDockItems: Array<object> = [
    {name: '收录', href: '/', count: 20, icon: 'star-o'},
    {name: '引用', href: '/', count: 13, icon: 'link'},
    {name: '收听', href: '/', count: 6, icon: 'headphones'},
    {name: '相簿', href: '/', count: 103, icon: 'image'}
  ];
  
  analysisItems: Array<object> = [
    {href: '/', name: '现实'},
    {href: '/', name: '早晨'},
    {href: '/', name: '星巴克'},
    {href: '/', name: '夕阳'},
  ];

  constructor() { }

  ngOnInit() {
  }

  onTabSwitch(tab: number) {
    this.currentTab = tab;
  }

  onCollect(id: number) {
    alert(id);
  }

  onWatch(id: number) {
    alert(id);
  }

  isRecommendTab(): Boolean {
    return Boolean(this.currentTab === TAB_RECOMMEND);
  }

  isNewTab(): Boolean {
    return Boolean(this.currentTab === TAB_NEW);
  }

  isToolsTab(): Boolean {
    return Boolean(this.currentTab === TAB_TOOLS);
  }

}
