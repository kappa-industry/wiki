import { Component, OnInit, HostListener } from '@angular/core';
import { ViewportScroller } from '@angular/common';
import { TREE_ACTIONS, KEYS, IActionMapping, ITreeOptions } from 'angular-tree-component';

@Component({
  selector: 'wiki-single-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  // indexes: Array<Object> = [
  //   {hash: 'sss', name: '发展历程'},
  //   {hash: 'sss', name: '发展历程'}
  // ];

  indexes: Array<Object> = [
    {
      id: '发展历程',
      name: '发展历程',
      // children: [
      //   { id: 2, name: 'child1' },
      //   { id: 'sss', name: 'child2' }
      // ]
    },
    {
      id: '编程开发',
      name: '编程开发',
      children: [
        { id: '编程工具', name: '编程工具' },
      ]
    }
  ];
  options = {
    isExpandedField: 'expanded',
    actionMapping: {
      mouse: {
        click:  (tree, node, $event) => {
          TREE_ACTIONS.TOGGLE_EXPANDED(tree, node, $event);
          this.scrollTo(node.data.id);
        }
      },
    },
  };

  constructor(
    public viewportScroller: ViewportScroller
  ) { }

  ngOnInit() {
    this.scrollListener(56);
  }

  scrollListener(height) {
    const wikiSingleIndex = document.getElementById('wiki-single-index');
    document.onscroll = (event) => {
      if (
        document.documentElement.scrollTop > height ||
        document.body.scrollTop > height
      ) {
        wikiSingleIndex.style.position = 'fixed';
        wikiSingleIndex.style.top = '0';
        wikiSingleIndex.style.left = 'auto';
      } else {
        wikiSingleIndex.style.position = 'static';
      }
    };
  }

  scrollTo(id: string) {
    this.viewportScroller.scrollToAnchor(id);
  }

  onScroll(event) {
    console.log(event);
  }

}
