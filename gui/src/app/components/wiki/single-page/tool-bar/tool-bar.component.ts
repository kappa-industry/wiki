import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../../../services/auth.service';

@Component({
  selector: 'wiki-tool-bar',
  templateUrl: './tool-bar.component.html',
  styleUrls: ['./tool-bar.component.scss']
})
export class ToolBarComponent implements OnInit {

  @Input() id: Number;
  @Input() userId: Number;

  constructor(
    public authService: AuthService
  ) { }

  ngOnInit() {
  }

  collectHandle(e) {
    alert(this.id);
  }

}
