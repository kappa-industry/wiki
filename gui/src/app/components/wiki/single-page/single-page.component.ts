import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'wiki-single-page',
  templateUrl: './single-page.component.html',
  styleUrls: ['./single-page.component.scss']
})
export class SinglePageComponent implements OnInit {

  // id: Number;
  id: Number;
  title="jQuery JavaScript ML";
  collect=20
  watch=24
  date="2018-10-31"
  userId=26
  userName="Sofia"

  constructor(
    public route: ActivatedRoute
  ) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.id = Number(id);
  }

}
