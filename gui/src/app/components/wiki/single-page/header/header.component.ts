import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'wiki-single-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() id: Number;
  @Input() title: String;
  @Input() collect: Number;
  @Input() watch: Number;
  @Input() date: String;
  @Input() userId: Number;
  @Input() userName: String;

  constructor() { }

  ngOnInit() {
  }

}
