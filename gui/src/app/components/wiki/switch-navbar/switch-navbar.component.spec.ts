import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwitchNavbarComponent } from './switch-navbar.component';

describe('SwitchNavbarComponent', () => {
  let component: SwitchNavbarComponent;
  let fixture: ComponentFixture<SwitchNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SwitchNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwitchNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
