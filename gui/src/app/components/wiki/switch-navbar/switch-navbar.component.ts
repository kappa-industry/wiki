import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ToolService } from '../../../services/tool.service';
import { TAB_RECOMMEND, TAB_NEW, TAB_TOOLS } from './constants';

@Component({
  selector: 'wiki-switch-navbar',
  templateUrl: './switch-navbar.component.html',
  styleUrls: ['./switch-navbar.component.scss']
})
export class SwitchNavbarComponent implements OnInit {

  @Input() onSwitch: Function;
  @Output() tab = new EventEmitter<any>();

  currentTab: number = TAB_RECOMMEND;

  constructor(
    public toolService: ToolService
  ) { }

  ngOnInit() {
    this.setTab(TAB_RECOMMEND);
  }

  isTab(tab: number): Boolean {
    return Boolean(this.currentTab === tab);
  }

  setTab(tab: number) {
    this.currentTab = tab;
    this.tab.emit(tab);
  }

  switchTab(event: MouseEvent, tab: number) {
    event.preventDefault();
    this.setTab(tab);
  }

  isRecommendTab(): Boolean {
    return this.isTab(TAB_RECOMMEND);
  }

  isNewTab(): Boolean {
    return this.isTab(TAB_NEW);
  }

  isToolsTab(): Boolean {
    return this.isTab(TAB_TOOLS);
  }

  switchRecommendTab(event: MouseEvent) {
    this.switchTab(event, TAB_RECOMMEND);
  }

  switchNewTab(event: MouseEvent) {
    this.switchTab(event, TAB_NEW);
  }

  switchToolsTab(event: MouseEvent) {
    this.switchTab(event, TAB_TOOLS);
  }

}
