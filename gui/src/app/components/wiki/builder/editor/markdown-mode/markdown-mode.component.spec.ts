import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkdownModeComponent } from './markdown-mode.component';

describe('MarkdownModeComponent', () => {
  let component: MarkdownModeComponent;
  let fixture: ComponentFixture<MarkdownModeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkdownModeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkdownModeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
