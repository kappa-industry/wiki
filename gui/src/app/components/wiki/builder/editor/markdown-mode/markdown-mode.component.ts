import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { AbstractModeComponent } from '../abstract-mode.component';

@Component({
  selector: 'wiki-markdown-mode',
  templateUrl: './markdown-mode.component.html',
  styleUrls: ['./markdown-mode.component.scss']
})
export class MarkdownModeComponent extends AbstractModeComponent {

  content: string = '';

  onGetContent(event: MouseEvent) {
    event.preventDefault();
    this.success.emit(this.content);
  }

}
