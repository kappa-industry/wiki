import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'wiki-code-mode',
  templateUrl: './code-mode.component.html',
  styleUrls: ['./code-mode.component.scss']
})
export class CodeModeComponent implements OnInit {

  @Output() success = new EventEmitter<any>();
  @Output() cancel = new EventEmitter<any>();

  languages: Array<String> = [
    'javascript',
    'css',
    'html',
    'xml',
  ];

  preview: Boolean = false;

  content: string = '';

  language: string = 'javascript';

  constructor() { }

  ngOnInit() {
  }

  onGetContent(event: MouseEvent) {
    event.preventDefault();
    this.success.emit(this.content);
  }

  onPreview(event: MouseEvent) {
    event.preventDefault();
    this.preview = true;
  }

  onEdit(event: MouseEvent) {
    event.preventDefault();
    this.preview = false;
  }

  onLanguageChange(event: MouseEvent, lang: string) {
    event.preventDefault();
    this.language = lang;
  }

  onCancel(event: MouseEvent) {
    event.preventDefault();
    this.cancel.emit();
  }

}
