import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodeModeComponent } from './code-mode.component';

describe('CodeModeComponent', () => {
  let component: CodeModeComponent;
  let fixture: ComponentFixture<CodeModeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodeModeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeModeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
