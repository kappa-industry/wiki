import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'wiki-editor-block',
  templateUrl: './block.component.html',
  styleUrls: ['./block.component.scss']
})
export class BlockComponent implements OnInit {

  @Input() class: string;

  switchBar: Boolean = false;

  switchName: Boolean = true;

  touchBar:Boolean = true;

  currentMode: String = null;

  codeData: string = '';

  constructor() { }

  ngOnInit() {
  }

  showSwitchBar() {
    this.switchName = false;
    this.switchBar = true;
  }

  hideSwitchBar() {
    this.switchName = true;
    this.switchBar = false;
  }

  hideInfoLayer() {
    this.switchBar = false
    this.switchName = false;
    this.touchBar = false;
  }

  showInfoLayer() {
    this.switchBar = true
    this.switchName = true;
    this.touchBar = true;
  }

  onCancel() {
    this.showInfoLayer();
    this.currentMode = null;
  }

  onText() {
    this.hideInfoLayer();
    this.currentMode = 'text';
  }

  onCode() {
    this.hideInfoLayer();
    this.currentMode = 'code';
  }

  onImage() {
    this.hideInfoLayer();
    this.currentMode = 'image';
  }

  onTable() {
    this.hideInfoLayer();
    this.currentMode = 'table';
  }

}
