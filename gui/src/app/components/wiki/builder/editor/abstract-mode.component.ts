import { OnInit, EventEmitter, Output } from '@angular/core';

export class AbstractModeComponent implements OnInit {

  @Output() success = new EventEmitter<any>();
  @Output() cancel = new EventEmitter<any>();

  preview: Boolean = false;

  constructor() { }

  ngOnInit() {
  }

  onEdit(event: MouseEvent) {
    event.preventDefault();
    this.preview = false;
  }

  onPreview(event: MouseEvent) {
    event.preventDefault();
    this.preview = true;
  }

  onCancel(event: MouseEvent) {
    event.preventDefault();
    this.cancel.emit();
  }

}
