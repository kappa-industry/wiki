import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'wiki-analysis-list',
  templateUrl: './analysis-list.component.html',
  styleUrls: ['./analysis-list.component.scss']
})
export class AnalysisListComponent implements OnInit {

  @Input() items: Array<object>;

  constructor() { }

  ngOnInit() {
  }

}
