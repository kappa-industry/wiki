import { Component, OnInit } from '@angular/core';
import { FormComponent } from '../form.component';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { LoginForm } from '../../forms/login.form';
import { ValidationErrors, FormControl } from '@angular/forms';
import { ToolService } from '../../services/tool.service';
import { APP_NAME_EN } from 'src/app/constants/client.const';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends FormComponent implements OnInit {

  form: LoginForm;

  captchaUrl: string;

  appName: string = APP_NAME_EN

  constructor(
    public authService: AuthService,
    public toolService: ToolService,
    public router: Router
  ) {
    super();
  }

  ngOnInit() {
    this.form = new LoginForm();
    this.refreshCaptcha();
  }

  /**
   * Refresh the captcha image.
   */
  refreshCaptcha() {
    this.captchaUrl = this.toolService.refreshCaptchaUrl();
  }

  /**
   * Submit for register.
   * @return Promise
   */
  onSubmit() {
    if (this.form.valid) {
      let promise: Promise<any> = this.authService.login(this.form.value);
      promise.then((data) => {
        alert('登录成功！');
        this.router.navigateByUrl('/');
      });
      return this.asyncErrorHandle(promise, error => {
        this.refreshCaptcha();
      });
    }
  }

}
