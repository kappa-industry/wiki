export interface CollectionBody {
  version: string;
  href: string;
}

export interface ResponseCollection {
  collection: CollectionBody;
}
