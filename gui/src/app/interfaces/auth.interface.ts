export interface LoginData {
  email: string;
  password: string;
  captcha: string;
}

export interface RegistData {
  email: string;
  name: string;
  password: string;
  rePassword: string;
  captcha: string;
}

export interface UserOptions {
  displayBulletin: Boolean;
}
