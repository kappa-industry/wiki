export const STORAGE_AUTH_KEY = '_auth';
export const STORAGE_AUTH_EXPIRES = '_expires';
export const STORAGE_USER_ID = '_userid';
export const APP_NAME_EN = 'Fences';
export const APP_NAME_CN = 'fences';
