import { ValidatorFn, AbstractControl } from "@angular/forms";

export function confirmPassword(password: AbstractControl): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    if (password.value === control.value) {
      return null;
    }
    return {'confirmPassword': true};
  };
}
