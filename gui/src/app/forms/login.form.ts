import { FormGroup, FormControl, Validators } from '@angular/forms';


export class LoginForm extends FormGroup {

  constructor() {

    const email = new FormControl(null, [
      Validators.required,
      Validators.email
    ]);
    const password = new FormControl(null, [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(16),
      // Validators.pattern(/^a-zA-Z0-9_&/)
    ]);
    const captcha = new FormControl(null, [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(6),
    ]);

    super({ email, password, captcha });
  }

  get email() { return this.get('email'); }
  get password() { return this.get('password'); }
  get captcha() { return this.get('captcha'); }

}
