import { FormGroup, FormControl, Validators } from '@angular/forms';
import { confirmPassword } from '../validators/confirm-password.validator';


export class RegisterForm extends FormGroup {

  constructor() {
    const name = new FormControl(null, [
      Validators.required,
      Validators.maxLength(16),
    ]);
    const email = new FormControl(null, [
      Validators.required,
      Validators.email
    ]);
    const password = new FormControl(null, [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(16),
      // Validators.pattern(/^a-zA-Z0-9_&/)
    ]);
    const rePassword = new FormControl(null, [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(16),
      confirmPassword(password)
    ]);
    const captcha = new FormControl(null, [
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(6),
    ]);

    super({name, email, password, rePassword, captcha});
  }

  get name() { return this.get('name'); }
  get email() { return this.get('email'); }
  get password() { return this.get('password'); }
  get rePassword() { return this.get('rePassword'); }
  get captcha() { return this.get('captcha'); }

}
