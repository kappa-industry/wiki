import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { BASE_URL } from '../constants/server.const';
import { Observable, throwError, Subscription } from 'rxjs';
import { catchError, retry, tap } from 'rxjs/operators';
import { STORAGE_AUTH_KEY } from '../constants/client.const';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  storage = {
    authToken: null
  };

  constructor(
    public http: HttpClient,
    public router: Router
  ) { }

  getHttpOptions(): object {
    const headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    const token = this.getAuthToken();
    if (token) {
      headers['Authorization'] = `Bearer ${token}`;
    }

    const httpOptions = {
      observe: 'response',
      headers: new HttpHeaders(headers)
    };
    return httpOptions;
  }

  /**
   * Set authorization token and expires date.
   * @param token auth token
   */
  setAuthorization(token: string) {
    this.storage.authToken = token;
    localStorage.setItem(STORAGE_AUTH_KEY, token);
  }

  /**
   * Get the authorization token from catch or localStorage.
   */
  getAuthToken(): string {
    if (this.storage.authToken) {
      return this.storage.authToken;
    }
    const token =   localStorage.getItem(STORAGE_AUTH_KEY);
    this.storage.authToken = token;
    return token;
  }

  /**
   * Remove current auth token.
   */
  removeAuthorization() {
    this.storage.authToken = null;
    localStorage.removeItem(STORAGE_AUTH_KEY);
  }

  /**
   * If has the authorization token here?
   */
  hasAuthToken(): Boolean {
    return Boolean(this.getAuthToken() !== null);
  }

  /**
   * Spliced a link address.
   * @param url part of url
   */
  getUrl(url: string): string {
    return `${BASE_URL}${url}`;
  }

  get(url: string): Promise<any> {
    const realUrl = this.getUrl(url);
    return this.http.get<any>(realUrl, this.getHttpOptions())
      .pipe(
        tap(data => this.handleSuccess(data)),
        catchError((err) => this.handleError(err))
      ).toPromise();
  }

  post(url: string, data: any): Promise<any> {
    const realUrl = this.getUrl(url);
    return this.http.post<any>(realUrl, data, this.getHttpOptions())
      .pipe(
        tap(data => this.handleSuccess(data)),
        catchError((err) => this.handleError(err))
      ).toPromise();
  }

  handleSuccess(response) {
    const token = response.headers.get('Authorization');
    if (token) {
      this.setAuthorization(token);
    }
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 401) {
      localStorage.clear();
      this.router.navigateByUrl('/login');
    }
    return throwError(error);
  };
}
