import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { LoginData, RegistData } from '../interfaces/auth.interface';
import { STORAGE_AUTH_KEY, STORAGE_USER_ID } from '../constants/client.const';
import { catchError } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ToolService } from './tool.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    public httpService: HttpService
  ) { }

  /**
   * Check if user is signin.
   */
  isLogin(): Boolean {
    return this.httpService.hasAuthToken();
  }

  isCurrentUser(id: Number): Boolean {
    const userId = Number(localStorage.getItem(STORAGE_USER_ID));
    return Boolean(userId === id);
  }

  ping(): Promise<any> {
    return this.httpService.get('/auth/ping').then(response => {
      return response.body;
    });
  }

  /**
   * User login.
   * @param data LoginData
   */
  login(data: LoginData): Promise<any> {
    return this.httpService.post('/auth/login', data).then(response => {
      const { id, token } = response.body;
      this.httpService.setAuthorization(token);
      localStorage.setItem(STORAGE_USER_ID, id);
      return response.body;
    }).catch(res => {
      console.error(res);
    });
  }

  register(data: RegistData): Promise<any> {
    return this.httpService.post('/auth/register', data).then(response => {
      return response.body;
    });
  }

  logout() {
    localStorage.clear();
    location.reload();
  }
}
