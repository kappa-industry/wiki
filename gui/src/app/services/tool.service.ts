import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ToolService {

  constructor(
    public authService: AuthService
  ) { }

  /**
   * Make and return time stamp
   * @return number
   */
  getTimeStamp(): number {
    const date = new Date();
    const timestamp = Date.parse(date.toString());
    return timestamp;
  }

  /**
   * Refresh the captcha image.
   */
  refreshCaptchaUrl(): string {
    const timestamp = this.getTimeStamp();
    return '/api/auth/captcha?_d=' + timestamp;
  }

  emptyLink(event: MouseEvent) {
    event.preventDefault();
    return '';
  }
}
