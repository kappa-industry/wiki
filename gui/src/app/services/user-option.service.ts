import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { UserOptions } from '../interfaces/auth.interface';

@Injectable({
  providedIn: 'root'
})
export class UserOptionService {

  options: UserOptions = {
    displayBulletin: true
  };

  constructor(
    public httpService: HttpService
  ) { }

  /**
   * Set a user option.
   * @param option The user option value.
   */
  setOption(option: object) {
    this.options = {
      ...this.options,
      ...option
    };
  }

  fetchOptions() {
    // this.options = value;
  }

  diableBulletin() {
    this.httpService.post('/user/option', {displayBulletin: false})
      .then(response => {
        if (response.status === 200) {
          this.setOption({displayBulletin: false});
        }
      });
  }
}
