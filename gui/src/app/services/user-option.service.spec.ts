import { TestBed, inject } from '@angular/core/testing';

import { UserOptionService } from './user-option.service';

describe('UserOptionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserOptionService]
    });
  });

  it('should be created', inject([UserOptionService], (service: UserOptionService) => {
    expect(service).toBeTruthy();
  }));
});
