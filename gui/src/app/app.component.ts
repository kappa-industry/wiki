import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';
import { APP_NAME_EN } from './constants/client.const';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = `${APP_NAME_EN} | 您的个人wiki！`;

  constructor(
    public authService: AuthService
  ) { }

  ngOnInit() {
    if (this.authService.isLogin()) {
      console.log('login');
    } else {
      console.log('not login')
    }
  }
}
