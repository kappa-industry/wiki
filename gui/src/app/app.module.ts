import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { MarkdownModule, MarkedOptions } from 'ngx-markdown';

import { HeaderComponent } from './components/header/header.component';
import { AppComponent } from './app.component';
import { HeaderModule } from './components/header/header.module';
import { RegisterModule } from './components/register/register.module';
import { LoginModule } from './components/login/login.module';
import { HomeModule } from './components/home/home.module';
import { WikiModule } from './components/wiki/wiki.module';
import { ToolsModule } from './components/tools/tools.module';
import { TreeModule } from 'angular-tree-component';

export const routes: Routes = [
  { path: 'header', component: HeaderComponent },
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    AngularFontAwesomeModule,
    TreeModule.forRoot(),
    MarkdownModule.forRoot({ loader: HttpClient }),
    HeaderModule,
    RegisterModule,
    LoginModule,
    HomeModule,
    WikiModule,
    ToolsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
